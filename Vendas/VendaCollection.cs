﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Vendas
{
    class VendaCollection
    {

        private static List<Venda> Vendas = new List<Venda>();

        public static void Insert(Venda Venda)
        {
            Venda.Id = Vendas.Count + 1;
            Vendas.Add(Venda);
        }

        public static Venda Search(Venda Venda)
        {
            foreach (Venda x in Vendas)
            {
                if (Venda.Id == x.Id)
                {
                    return x;
                }
            }
            return null;
        }

        public static List<Venda> GetVendas()
        {
            return Vendas;
        }
    }
}
