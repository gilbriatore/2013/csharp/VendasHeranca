﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Vendas
{
    class ItemVenda
    {
        public Produto Produto { set; get; }
        public int Quantidade { set; get; }
        public float Unitario { set; get; }
    }
}
