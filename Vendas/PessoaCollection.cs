﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Vendas
{
    class PessoaCollection
    {
        private static List<Pessoa> pessoas = new List<Pessoa>();

        public static void Insert(Pessoa pessoa)
        {
            pessoa.Id = pessoas.Count() + 1;
            pessoas.Add(pessoa);
        }

        public static Pessoa Search(Pessoa pessoa)
        {
            return pessoas.FirstOrDefault(x => x.Cpf.Equals(pessoa.Cpf));
        }
    }
}
