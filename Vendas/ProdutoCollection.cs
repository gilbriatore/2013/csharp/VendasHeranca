﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Vendas
{
    class ProdutoCollection
    {
        private static List<Produto> produtos = new List<Produto>();

        public static void Insert(Produto produto)
        {
            produto.Id = produtos.Count() + 1;
            produtos.Add(produto);
        }

        public static Produto Search(Produto produto)
        {
            foreach (Produto x in produtos)
            {
                if (x.Nome.Equals(produto.Nome))
                {
                    return x;
                }
            }
            return null;
        }
    }
}
